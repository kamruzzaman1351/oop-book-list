// Variable From UI
const bookFormUI = document.getElementById("book-form"),
      bookTitleUI = document.getElementById("title"),
      bookAutherUI = document.getElementById("auther"),
      bookIsbnUI = document.getElementById("isbn"),
      bookListTableUI = document.getElementById("book-list"),
      bookContainerUI = document.querySelector(".container");

// Content Load Event
document.addEventListener("DOMContentLoaded", () => {
    Storage.displayBooks();
});
// Form Submit Event Listner
bookFormUI.addEventListener("submit", bookFormSubmit);
// Remove Book from UI Event Listner
bookListTableUI.addEventListener("click", (e) => {
    e.preventDefault();
    const ui = new UI();
    ui.deleteBook(e);
});

// Form Submit Event
function bookFormSubmit(e) {    
    e.preventDefault();
    const title = bookTitleUI.value,
          author = bookAutherUI.value, 
          isbn = bookIsbnUI.value,
          ui = new UI(); 
    if(title === "" || author === "" || isbn === "") {
        ui.showMassageDiv("Please Fill all the inputs", "error");
        
    } else {
        const book = new Book(title, author, isbn);
        Storage.storeBooks(book); 
        ui.showBookList(book);
        ui.showMassageDiv("Book Added to the list", "success");
        ui.clearInputField();
    }
    
}


// Book Class
class Book {
    constructor(title, author, isbn) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    }
    
}

// UI Class
class UI {
    // Show Massage in ui
    showMassageDiv(msg, className){
        const div = document.createElement("div");
        div.className = className;
        div.appendChild(document.createTextNode(msg));
        bookContainerUI.insertBefore(div, bookFormUI);
        setTimeout(() => {
            document.querySelector(`.${className}`).remove()
        },3000);
    }
    // Show books in ui
    showBookList(book) {
        const tr = document.createElement("tr");
        tr.innerHTML = `
                        <td>${book.title}</td>
                        <td>${book.author}</td>
                        <td>${book.isbn}</td>
                        <td>
                        <a class="button delete" href="#">Delete</a>
                        </td>`;
        bookListTableUI.appendChild(tr);
    }
    // Clear Input Field
    clearInputField() {
        bookTitleUI.value = '';
        bookAutherUI.value = ''; 
        bookIsbnUI.value = '';
    }
    // Dlete Book from UI
    deleteBook(e) {
        if(e.target.classList.contains("delete")) {
            if(confirm("Are you sure?")) {
                e.target.parentElement.parentElement.remove();
                this.showMassageDiv("Book is remove from the list", "success");
                const isbn = e.target.parentElement.previousElementSibling.textContent;                
                Storage.removeBook(isbn);
            }
         }
    }
}

// Storage Class (Add Book Data to Local Storage)
class Storage {
    // Get Books From LS
    static getBooks() {
        let books;
        if(localStorage.getItem("books") === null) {
            books = [];
        } else {
            books = JSON.parse(localStorage.getItem("books"));
        }
        return books;
    }

    // Add Book in LS
    static storeBooks(book) {
        const books = Storage.getBooks();        
        books.push(book);
        localStorage.setItem("books", JSON.stringify(books));
    }

    // Display Books in UI
    static displayBooks() {
        const books = Storage.getBooks();
        const ui = new UI();
       if(books !== null) {
            books.forEach((book) => {
                ui.showBookList(book);
            })
       }
    }

    // Remove Book from LS
    static removeBook(isbn) {
        const books = Storage.getBooks();
        books.forEach((book, index, books) => {
            if(book.isbn === isbn) {
                books.splice(index, 1);
            }
        });
        localStorage.setItem("books", JSON.stringify(books));
        
    }
}
