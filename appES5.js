// Variable From UI
const bookFormUI = document.getElementById("book-form"),
      bookTitleUI = document.getElementById("title"),
      bookAutherUI = document.getElementById("auther"),
      bookIsbnUI = document.getElementById("isbn"),
      bookListTableUI = document.getElementById("book-list"),
      bookContainerUI = document.querySelector(".container");

// Content Load Event
document.addEventListener("DOMContentLoaded", () => {
    const show = new Storage();
    show.displayBooks();
})
// Form Submit Event Listner
bookFormUI.addEventListener("submit", bookFormSubmit);
// Remove Book from List Event Listner
bookListTableUI.addEventListener("click", (e) => {
    const ui = new UI();
    ui.removeBook(e);
});
// Book Form Submit Event
function bookFormSubmit(e) {
    e.preventDefault();
    const bookTitle = bookTitleUI.value;
    const bookAuthor = bookAutherUI.value;
    const bookISBN = bookIsbnUI.value;
    const ui = new UI();
    if(bookTitle === '' || bookAuthor === '' || bookISBN === '') {
        ui.showMassageDiv("Please Fill all the input fields", "error");
    } else {
        const book = new Book(bookTitle, bookAuthor, bookISBN);
        ui.showBookList(book);
        ui.showMassageDiv("Book Added to the list", "success");
        // Store Book in LS
        const store = new Storage();
        store.storeBooks(book);
        ui.clearInputField();
    }
}

// Book Constructor
function Book(title, author, isbn) {
    this.title = title;
    this.author = author;
    this.isbn = isbn;
}

// UI Constractor 
function UI() {
    // 
}
// UI Prototype. Success or Error Massage Show
UI.prototype.showMassageDiv = function (msg, className) {
    const div = document.createElement("div");
    div.className = className;
    div.appendChild(document.createTextNode(msg));
    bookContainerUI.insertBefore(div, bookFormUI);
    setTimeout(() => {
        document.querySelector(`.${className}`).remove();
    },3000);
}
// UI Prototype . Show Book in the ui
UI.prototype.showBookList = function (book) {
    const tr = document.createElement("tr");
        tr.innerHTML = `
                        <td>${book.title}</td>
                        <td>${book.author}</td>
                        <td>${book.isbn}</td>
                        <td>
                        <a class="button delete" href="#">Delete</a>
                        </td>`;
    bookListTableUI.appendChild(tr);
}
// UI Prototype. Clear Input field
UI.prototype.clearInputField = function () {
    bookTitleUI.value = '';
    bookAutherUI.value = '';
    bookIsbnUI.value = '';
}
// UI Prototype. Remove Book from Ui
UI.prototype.removeBook = function (e) {
   if(e.target.classList.contains("delete")) {
      if(confirm("Are you sure?")) {
        e.target.parentElement.parentElement.remove();
        this.showMassageDiv("Book is removed from the list", "success");        
        const isbn = e.target.parentElement.previousElementSibling.textContent;
        const storeRemove = new Storage();
        storeRemove.removeBook(isbn);
      }
   }
}

// Storage Constructor
function Storage() {
    // 
}
//Stroage Prototype. Get Books From LS 
Storage.prototype.getBooks = function() {
    let books;
    if(localStorage.getItem("books") === null) {
        books = [];
    } else {
        books = JSON.parse(localStorage.getItem("books"));
    }
    return books;
}
//Stroage Prototype. Add Book in LS
Storage.prototype.storeBooks = function(book) {
    const books = this.getBooks();        
    books.push(book);
    localStorage.setItem("books", JSON.stringify(books));
}
//  Stroage Prototype. Display Books in UI
Storage.prototype.displayBooks = function() {
    const books = this.getBooks();
    const ui = new UI();
    if(books !== null) {
        books.forEach((book) => {
            ui.showBookList(book);
        })
    }
}

// Stroage Prototype. Remove Book from LS
Storage.prototype.removeBook = function(isbn) {
    const books = this.getBooks();
    books.forEach((book, index, books) => {
        if(book.isbn === isbn) {
            books.splice(index, 1);
        }
    });
    localStorage.setItem("books", JSON.stringify(books));
}
